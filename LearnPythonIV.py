import useful_tools
# Part 22 - Try / Except (Exception Handling, raise included)
try:
    operand1 = float(input("Enter first number: "))
    operator = input("Enter operator: ")
    operand2 = float(input("Enter second number:"))

    if operator == "+":
        print(operand1 + operand2)
    elif operator == "-":
        print(operand1 - operand2)
    elif operator == "*":
        print(operand1 * operand2)
    elif operator == "/":
        print(operand1 / operand2)
    else:
        raise ValueError(operator + " is not an operator")
except ZeroDivisionError as err:
    print("Cannot divide by zero!")
    print(err)
except ValueError as err:
    print("Invalid input!")
    print(err)

# Part 23 - Reading from Files
transaction_file = open("transactions.txt", "r")

print(transaction_file.readable())
print(transaction_file.read())

transaction_file.close()

# Part 24 - Writing to Files
transaction_file = open("transactions.txt", "a")  # 'a' in second parameter for appending to file, 'w' to overwrite file

transaction_file.write("+$200 --- 1/30\n")

transaction_file.close()

# Part 25 - Importing Modules (import statements are always at the top of the file)
print(useful_tools.roll_dice(100))
# Modules are either built into Python or are external Python files (check External Libraries for examples)
# 3rd party external modules can be installed using pip package manager. There are plenty of modules out there!

# Part 26 - Classes & Objects


class Employee:
    def __init__(self, name, title, salary, is_working_remotely):
        self.name = name
        self.title = title
        self.salary = salary
        self.is_working_remotely = is_working_remotely

# You would need to use 'from (file) import (class)' statement if referencing class from different file


employee1 = Employee("John", "Senior Developer", 85000, False)
employee2 = Employee("Jake", "Customer Service Representative", 50000, False)
print(employee1.salary)
print(employee2.title)

# Part 27 - Multiple Choice Quiz
prompts = [
    "How often does a leap year happen?\n(a) Every 3 Years\n(b) Every 4 Years\n(c) Every 5 Years\n\n",
    "Which ocean is the largest?\n(a) Atlantic\n(b) Indian\n(c) Pacific\n\n",
    "Which of the following animals is not a bird?\n(a) Gopher\n(b) Ostrich\n(c) Falcon\n\n"
]


class Question:
    def __init__(self, prompt, answer):
        self.prompt = prompt
        self.answer = answer


answer_key = [
    Question(prompts[0], "b"),
    Question(prompts[1], "c"),
    Question(prompts[2], "a")
]


def run_quiz(questions):
    score = 0
    for question in questions:
        answer = input(question.prompt)
        if answer == question.answer:
            score += 1
    print(str(score) + "/" + str(len(answer_key)) + " Correct")


run_quiz(answer_key)

# Part 28 - Object Functions


class Computer:
    def __init__(self, cpu, gpu, ram, storage):
        self.cpu = cpu
        self.gpu = gpu
        self.ram = ram
        self.storage = storage

    def can_store_aaa_game(self):
        if self.storage > 100:
            return True
        else:
            return False


computer1 = Computer("Ryzen 5 3600", "RX 580", 16, 120)

print(computer1.can_store_aaa_game())

# Part 29 - Inheritance


class Car:
    def start(self):
        print("the car starts up")

    def shift(self):
        print("the car shifts gear")

    def special_action(self):
        print("the car immediately stops")


class Racecar(Car):
    def special_action(self):
        print("the car activates its turbo boosters")

    def drift(self):
        print("the car drifts")


car1 = Car()
car2 = Racecar()

car1.special_action()
car2.drift()
car2.special_action()

# Part 30 - Python Interpreter

# The Python Interpreter exists to quickly test Python code. It is a command line application.
# Type 'help' for more information

# That about sums it up for the remaining content of Learn Python: https://youtu.be/rfscVS0vtbw
