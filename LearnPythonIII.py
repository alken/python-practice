# Part 13 - 4-Function Calculator
operand1 = float(input("Enter first number: "))
operator = input("Enter operator: ")
operand2 = float(input("Enter second number:"))

if operator == "+":
    print(operand1 + operand2)
elif operator == "-":
    print(operand1 - operand2)
elif operator == "*":
    print(operand1 * operand2)
elif operator == "/":
    print(operand1 / operand2)
else:
    print("Invalid operator!")

# Part 14 - Dictionaries
some_stuff = {
    "Tree": "Maple",
    "Snake": "Boa Constrictor",
    "Bird": "Bald Eagle",
}

print(some_stuff["Tree"])
print(some_stuff.get("Snake"))
print(some_stuff.get("Fish", "This key is not available"))

# Part 15 - While Loop
i = 1
while i <= 10:
    print("Print count: " + str(i))
    i += 1

print("Loop is completed")

# Part 16 - Guessing Game
secret_word = "pineapple"
guess = ""
guess_count = 0
guess_limit = 5
out_of_guesses = False

while guess != secret_word and not out_of_guesses:
    if guess_count < guess_limit:
        guess = input("Guess the secret word: ")
        guess_count += 1
    else:
        out_of_guesses = True

if out_of_guesses:
    print("You lose!")
else:
    print("You win! It took you " + str(guess_count) + " guesses.")

# Part 17 - For Loop
things = ["Baseball", "Skateboard", "Cellphone"]
for character in "For Loop":
    print(character)

for thing in things:
    print(thing)

for index in range(len(things)):
    print(things[index])

for iteration in range(len(things)):
    if iteration == 0:
        print("First iteration!")
    else:
        print("Not the first iteration.")

# Part 18 - Exponents


def power(base_num, pow_num):
    result = 1
    for loop in range(pow_num):
        result *= base_num
    return result


print(power(4, 3))

# Part 19 - 2D Lists and Nested Loops
number_grid = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [0]
]

print(number_grid[0][2])

for row in number_grid:
    for column in row:
        print(column)

# Part 20 - Fake Language


def translate(phrase):
    translation = ""
    for letter in phrase:
        if letter.upper() in "E":
            if letter.isupper():
                translation = translation + "Ee"
            else:
                translation = translation + "ee"
        elif letter.upper() in "O":
            if letter.isupper():
                translation = translation + "Oo"
            else:
                translation = translation + "oo"
        else:
            translation = translation + letter
    return translation


print(translate(input("Enter a phrase: ")))

# Part 21 - Comments
# Comments have already been implemented for every part, including this one.
# Python comments can be written using pound (#) symbols at the beginning of the line.
# Comments over multiple lines can be written between two lines of three single quotation marks (''').
# However, the above method of commenting is not the industry standard.
# Overall, comments in Python can be applied the same way as comments in many other high-level languages.

# That about sums it up for the third hour of Learn Python: https://youtu.be/rfscVS0vtbw
