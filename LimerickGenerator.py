# Limerick Generator

# Conditions (sorted by priority):
# {two syllable name}, {one syllable noun}, and {three syllable noun} must rhyme
# {two syllable verb} and {another two syllable verb} must rhyme
# if {one syllable verb} ends with an 'e', remove the last character
# if {two syllable verb} ends with an "ey", replace the last two characters with "i"
# if {two syllable verb} ends with an 'e', remove the last character
# if {two syllable verb} ends with an 'y', replace the last character with 'i'
# if {another two syllable verb} ends with an "ey", replace the last two characters with "i"
# if {another two syllable verb} ends with an 'e', remove the last character
# if {another two syllable verb} ends with an 'y', replace the last character with 'i'
# if {another one syllable verb} ends with an "ey", replace the last two characters with "i"
# if {another one syllable verb} ends with an 'e', remove the last character
# if {another one syllable verb} ends with an 'y', replace the last character with 'i'

# Instructions:
# 1 - Instantiate lists of possible words
# 2 - Randomly choose words and apply the conditions above
# 3 - Print the limerick

# 1
tsn_ay = []
tsn_ee = []
tsn_eh = []
tsn_er = []
tsn_i = []
tsn_oh = []
tsn_uh = []
two_syllable_name = [tsn_ay, tsn_ee, tsn_eh, tsn_er, tsn_i, tsn_oh, tsn_uh]
one_syllable_verb = []


# 2

# 3
print("There once was a robot named {two syllable name}")
print("It always enjoyed {one syllable verb}ing {one syllable adjective} {one syllable noun}")
print("But one day it {two syllable verb}ed")
print("And everyone {another two syllable verb}ed")
print("So {two syllable name} just {another one syllable verb}ed for {three syllable noun}")
