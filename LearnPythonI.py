# Part 1 - Hello World
print("Hello World!")  # Humble Beginnings

# Part 2 - Strings
some_name = "Leo"  # String
some_age = "50"  # String
some_phrase = "There's not much to see here"  # String
print("Meet " + some_name + ".")  # Putting a string variable in a print statement by concatenation
print(some_name + " is " + some_age + " years old.")  # Multiple string variables can be put in a print statement
print("\"" + some_phrase + "\" said " + some_name + ".")  # Use a backslash for special characters
print("\"" + some_phrase.upper() + "\" said " + some_name + " in frustration.")  # There are also string functions

# Part 3 - Numbers
print("Here is a number: ")
print(20)  # No need to distinguish between integers and floating points in print statements
some_num = 5  # Number
print(str(some_num) + " is the favorite number of " + some_name)  # Convert the number variable to string to print
print("When multiplied by 3, that number becomes " + (str(some_num * 3)))  # Arithmetic operations can be used

# Part 4 - User Input
user_name = input("What is your name?")  # Prompt the user to enter an input and store the input in a variable
user_age = input("How old are you?")  # Do it again
print("Hello " + user_name + "! It must be nice to be " + user_age + " years old.")  # Output both user inputs
some_x = input("Want to do some addition? Enter the first number.")  # Getting user input again
some_y = input("Now enter the second number.")  # And again
some_sum = float(some_x) + float(some_y)  # Getting the sum of user inputs by converting them to floating point values
print("The sum of " + some_x + " and " + some_y + " is " + str(some_sum))  # Printing the result

# Part 5 - Mad Libs Game
color = input("Enter a color: ")
plural_noun = input("Enter a plural noun: ")
food = input("Enter a food: ")
print("Roses are " + color)
print(plural_noun + " are blue")
print("I love " + food)

# That about sums it up for the first hour of Learn Python: https://youtu.be/rfscVS0vtbw
