# Part 6 - Lists
friends = ["Mia", "Kia", "Dia", "Ria", "Sia"]
mix = [123, "abc", -6, 'x']
print(friends[2])
print(friends[1:3])
print(friends[0:4])
print(mix)
print()

# Part 7 - List Functions
lucky_numbers = [5, 12, 3, 44, 78, 29, 60, 8]
lucky_numbers2 = lucky_numbers.copy()
lucky_numbers.insert(1, 14)
lucky_numbers.remove(78)
lucky_numbers.pop()
lucky_numbers.sort()
lucky_numbers.reverse()
print(lucky_numbers)
print(lucky_numbers2)
print()

# Part 8 - Tuples
coordinates = (4, 5)
coordinate_list = [coordinates, (5, 6), (9, 8)]
print(coordinates[0])
print(coordinates[1])
print(coordinate_list)
print()


# Part 9 - Functions
def greeter(name):
    print("Hello, " + name)


print("Top")
greeter("User")
print("Bottom")
print()


# Part 10 - Return Statement
def cube(num):
    return num*num*num


print(cube(4))
print()

# Part 11 - If, Else-If, and Else Statements
is_old = True
uses_facebook = True

if is_old and uses_facebook:
    print("You are very old")
elif is_old or uses_facebook:
    print("You are old")
else:
    print("You are young")
print()


# Part 12 - Comparisons
def max_num(num1, num2, num3):
    if num1 >= num2 and num1 >= num3:
        return num1
    elif num2 >= num1 and num2 >= num3:
        return num2
    else:
        return num3


print(max_num(6, 4, 22))
print()

# That about sums it up for the second hour of Learn Python: https://youtu.be/rfscVS0vtbw
